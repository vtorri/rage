#ifndef _UTIL_H__
#define _UTIL_H__ 1

Eina_Bool  util_video_ok(const char *path);
Eina_Bool  util_audio_ok(const char *path);
char      *util_videos_dir_get(void);

#endif
